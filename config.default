# ---------------------------------------------------------
# GoGo File Server Config file
# author: Tyler Egeto, tyleregeto.com
# ---------------------------------------------------------
# Note: Server must be restarted for changes to take affect.
# Note: This file (config.default) is not read by the application, it is a template file only.
# to specify a template file, copy & rename this file to `config` and the application will read it in.
# Note: This file is auto generated and replaced after every build.
# Note: when setting gflag values, 1 == on, 0 == off
# ---------------------------------------------------------

# the TCP network address the file server will listen on.
# Note: There is currently no support for HTTPS file serving. If someone is using
# this, and wants/needs it, please get touch, its an easy add!
listen_address=:10500

# should file responses be gzip'd?
# gzip is a little intellegent, client must advertise it. Some files are never zipped,
# such as images.
gzip_enabled=1

# if one, and the client advertises webp support, the file server will auto convert jpg and png
# files to webp versions. More information on webp can be found here: https://developers.google.com/speed/webp/
webp_enabled=1

# The root folder where all serverd files must reside from.
base_file_location=./files/

# If on, the application will clean up any files within the base_file_location
# that it has auto generated, such as webp conversions. Its always a complete
# wipe.
clear_cache=1

# If on, this generates webp file conversions, and gzips files at startup.
# This option allows these processes to be skipped at on the first request.
# Files are always cached after the first request regardless.
build_cache=1

# If on, the file server will run through its entire startup process, but then
# exit without serving any requests. This may be useful to pre-process files, or
# to run against a file location already being served by another instance after
# files have been added or altered.
quit_before_listen=0