package fileserver

import (
	"net/http"
	"path"
	"path/filepath"
	"os"
	"os/exec"
	"io"
	"time"
	"strings"
	"compress/gzip"
	"fmt"
)

// TODO make using the cache optional
// TODO support clearCache and build_cache config options

// WebpEnabled tells the file server auto convert PNG and JPG images to WEBP when the client supports it.
var WebpEnabled bool = true
// GzipEnabled tells the file server to send responses GZipped when it makes sense to do so.
var GzipEnabled bool = true
// BaseFolder is the folder the server looks for files in. It does not allow files
// to be returned from outside this location.
var BaseDir = "./files/"
// cacheDir is the location generated files are stored to prevent re-generating them
// for each request, for example PNG to WEBP conversions. It is always relative to
// BaseDir
var cacheDirName = ".fscache"
// cacheDir is the value of BaseDir combined with cacheDirName.
// Not accessible until after PrepareForUse gets called.
var cacheDir string

// GZipWriter add GZip compression to the files when responding to HTTP request.
// The requesting agent must advertise gzip support.
type GZipWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w GZipWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

// PrepareForUse ensures that BaseDir & cacheDir exist, and that cacheDir.
// This will attempt to create the directories if they are missing.
func PrepareForUse() error {
	// Ensure that it has a trailing slash, but only if it is specified,
	// otherwise it would reference the file system root.
	if len(BaseDir) > 0 && BaseDir[len(BaseDir)-1] != '/' {
		BaseDir += "/"
	}

	cacheDir =  BaseDir + cacheDirName
	return os.MkdirAll(cacheDir, 0777)
}

// ServeFile handles the incoming file requests and responds.
func ServeFile(w http.ResponseWriter, r *http.Request) {
	file := r.URL.RequestURI()

	f := path.Clean(file)
	i, err := os.Stat(BaseDir + f)

	if(err == nil && !i.IsDir()) {
		// set the expiry time one year from now
		t := time.Now()
		t = t.AddDate(1, 0, 0)
		w.Header().Add("Expires", t.Format(time.RFC1123))
		// Important, between GZip and auto image conversion, down steam cache's
		// need this header to know the encoding can change.
		w.Header().Set("Vary", "Accept-Encoding")
		// Check to see what features the client supports
		acceptsGzip := strings.Contains(r.Header.Get("Accept-Encoding"), "gzip")
		acceptsWebp := strings.Contains(r.Header.Get("Accept"), "image/webp")

		// route requests by the file extension
		ext := path.Ext(f)
		ext = strings.ToLower(ext)

		// if the client accepts webp, and its a image type we can convert to webp
		// we handle of responding to another handler
		if acceptsWebp && WebpEnabled {
			switch ext {
			case ".png", ".jpg", ".jpeg":
				serveImage(f, w, r)
				return
			}
		}

		// Check if we should gzip the response, if so hand it of to another handler
		if acceptsGzip && GzipEnabled {
			switch ext {
			case ".css", ".html", ".txt", ".js", ".xml", ".json", ".csv", ".sql":
				serveGzipFile(f, w, r)
				return
			}
		}

		// If nothing else has handled the file at this point, just serve it up.
		http.ServeFile(w, r, BaseDir + f)

	} else {
		http.NotFound(w, r)
	}
}

// serveImage servers PNG and JPG images only right now. Other images
// are handled as regular files. If the client advertise support, this
// servers WEBP images instead of PNG or JPG.
// We know the file exists before this function is called.
func serveImage(path string, w http.ResponseWriter, r *http.Request) {
	// check if a cached copy of the zipped file already exists
	// If it does, respond with it. Else make it, then respond.
	dir := cacheDir + filepath.Dir(path)
	file := filepath.Base(path) + ".gzip"
	cacheFileName := dir + "/" + file
	cacheFileExists := true

	// if the file doesn't exist, create it
	_, err := os.Stat(cacheFileName)
	if err != nil {
		cmd := exec.Command("utils/cwebp")
		// TODO run cmd, make file, /
	}

	// serve the cached version or fall back to the original gzipped on the fly
	if cacheFileExists {
		http.ServeFile(w, r, cacheFileName)
	} else {
		http.ServeFile(w, r, BaseDir + path)
	}
}

func serveGzipFile(path string, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Encoding", "gzip")
	w.Header().Set("Content-Type", "text/html")
	// the name of the cached gzip version of the file
	dir := cacheDir + filepath.Dir(path)
	file := filepath.Base(path) + ".gzip"

	cacheFileExists := true
	cacheFileName := dir + "/" + file

	// if the file doesn't exist, create it
	_, err := os.Stat(cacheFileName)
	if err != nil {
		// Make sure the path exists
		os.MkdirAll(dir, 0777)

		// FIXME we should write the new file in a tmp location to ensure
		// that it isn't returned before completed. Copy over when done.
		newFile, newFileErr := os.Create(cacheFileName)
		existingFile, existingFileErr := os.Open(BaseDir + path[1:])

		// if we created the new file successfully, gzip the original into it
		if newFileErr == nil && existingFileErr == nil {
			gz := gzip.NewWriter(newFile)
			_, copyErr := io.Copy(gz, existingFile)
			gz.Close()

			if copyErr != nil {
				cacheFileExists = false
				fmt.Println(copyErr)
			}
		} else {
			fmt.Println(existingFileErr)
			fmt.Println(newFileErr)
			cacheFileExists = false
		}

		// delete the tmp file if we failed above
		if newFile != nil && !cacheFileExists {
			os.Remove(cacheFileName)
		}
	}

	// serve the cached version or fall back to the original gzipped on the fly
	if cacheFileExists {
		http.ServeFile(w, r, cacheFileName)
	} else {
		gz := gzip.NewWriter(w)
		g := GZipWriter{Writer:gz, ResponseWriter:w}
		http.ServeFile(g, r, BaseDir + path)
		gz.Close()
	}
}