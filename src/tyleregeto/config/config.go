// Config is a simple configuration library that supports a key=value syntax for
// loading from a text file. Lines in config files that begin with `#` are treated
// as comments, and ignored. Eg:
//
// # this is a comment
// my_key=my value
// my_other_key=1
package config

import (
	"io/ioutil"
	"strconv"
	"strings"
)

type Config struct {
	values map[string]interface{}
}

func NewConfig() Config {
	return Config{values:make(map[string]interface{})}
}

func (c Config) Set(key string, val interface{}) {
	c.values[key] = val
}

func (c Config) AsInt(key string, def int) int {
	v, ok := c.values[key]
	if ok {
		switch v.(type) {
			case bool:
				if v.(bool) {
					return 1
				} else {
					return 0
				}
			case int:
				return v.(int)
			case string:
				i, err := strconv.Atoi(v.(string))
				if err != nil {
					return i
				}
		}
	}
	return def
}

func (c Config) AsBool(key string, def bool) bool {
	v, ok := c.values[key]
	if ok {
		switch v.(type) {
			case bool:
				return v.(bool)
			case int:
				return v.(int) == 1
			case string:
				return v.(string) == "1"
		}
	}
	return def
}

func (c Config) AsStr(key string, def string) string {
	v, ok := c.values[key]
	if ok {
		switch v.(type) {
			case bool:
				if v.(bool) {
					return "1"
				} else {
					return "0"
				}
			case int:
				return strconv.FormatInt(v.(int64), 10)
			case string:
				return v.(string)
		}
	}
	return def
}

func ParseString(str string) Config {
	var c Config = NewConfig()
	// FIXME window line endings
	lines := strings.Split(string(str), "\n")
	for _, line := range lines {
		// skip comments
		if len(line) <= 1 || line[0] == '#' {
			continue
		}

		// split the line into key/val
		pieces := strings.SplitN(line, "=", 2)
		if len(pieces) != 2 {
			continue
		}

		c.Set(pieces[0], pieces[1])
	}
	return c
}

// ParseFile laods a file from disk and converts it to a Config struct
func ParseFile(file string) (Config, error) {
	// Note: this laods the hole file into memory, not overly efficent, however,
	// our config files are generally small enough that its acceptable. If large
	// config files are needed, this should be improved.
	txt, err := ioutil.ReadFile(file)
	if err != nil {
		return Config{}, err
	}
	return ParseString(string(txt)), nil
}