package main

import (
	"fmt"
	"net/http"
	"tyleregeto/fileserver"
	"tyleregeto/config"
)

func main() {
	// load the config file
	c, err := config.ParseFile("config")
	if err != nil {
		fmt.Println("Warning: config file not found, using defaults.")
	}

	// apply config values, fallback to defaults
	fileserver.BaseDir = c.AsStr("base_file_location", "files")
	fileserver.WebpEnabled = c.AsBool("webp_enabled", true)
	fileserver.GzipEnabled = c.AsBool("gzip_enabled", true)

	// tell the file server to prepare itself. If any errors occur here, we stop immediately.
	err = fileserver.PrepareForUse();
	if err != nil {
		fmt.Println("err: " + err.Error())
		return
	}

	if !c.AsBool("quit_before_listen", false) {
		var adde string = c.AsStr("listen_address", ":10500")
		http.HandleFunc("/", fileserver.ServeFile)
		err = http.ListenAndServe(adde, nil)
	}

	// log any errors. Output goes into a file specified by startup script.
	if err != nil {
		fmt.Println("err: " + err.Error())
	}
}