#!/bin/bash

# make the `build` dir, and the `files` dir.
# `files` is the default location all files are stored
mkdir -p build/files/

# set the GO_PATH var to be the cwd
export GOPATH=$(pwd)

# build the the file
go build -o build/fileserver src/main.go

cp scripts/startup.sh build/startup.sh
cp config.default build/config.default
cp utils build/utils